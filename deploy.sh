#!/bin/bash
cd `dirname $0`

rsync -a --delete public/ dist/
npm run build
npm run create-sitemap
rsync -ac --delete dist/ lolipop:~/web/iidx/
npm run ping-sitemap
