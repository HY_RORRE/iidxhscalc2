#!/bin/bash
cd `dirname $0`
cd static/img/icons

if [ $# -ne 1 ]; then
  echo "Specify icon-file with argument." 1>&2
  exit 1
fi

if [ ! `which convert` ]; then
  echo "Install imagemagick before."
  exit 1
fi

convert $1 -resize 192x192 android-chrome-192x192.png
convert $1 -resize 512x512 android-chrome-512x512.png
convert $1 -resize 180x180 apple-touch-icon.png
convert $1 -resize 60x60 apple-touch-icon-60x60.png
convert $1 -resize 76x76 apple-touch-icon-76x76.png
convert $1 -resize 120x120 apple-touch-icon-120x120.png
convert $1 -resize 152x152 apple-touch-icon-152x152.png
convert $1 -resize 180x180 apple-touch-icon-180x180.png
convert $1 -resize 48x48 favicon.ico
convert $1 -resize 16x16 favicon-16x16.png
convert $1 -resize 32x32 favicon-32x32.png
convert $1 -resize 144x144 msapplication-icon-144x144.png
convert $1 -resize 150x150 mstile-150x150.png
