let sm = require('sitemap')
let fs = require('fs')
let path = require('path')

let sitemap = sm.createSitemap({
  hostname: 'https://iidx.hyrorre.com',
  cacheTime: 600000, //600 sec (10 min) cache purge period
  urls: [
    { url: '/' , changefreq: 'weekly', priority: 1.0, lastmodrealtime: true, lastmodfile: 'src/assets/musics.csv' },
  ]
})

let dir = path.resolve(__dirname, '../dist')
fs.existsSync(dir) || fs.mkdirSync(dir)
fs.writeFileSync(path.resolve(dir, 'sitemap.xml'), sitemap.toString())
