let sitemap_url = 'https%3A%2F%2Fiidx.hyrorre.com%2Fsitemap.xml'
let engines = ['https://www.google.com', 'https://www.bing.com']

let XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest
let xhr = new XMLHttpRequest()

engines.forEach(engine => {
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        let data = xhr.responseText
        console.log('Successful ping of ' + engine)
      } else {
        console.log('Failure ping of ' + engine)
        console.dir(xhr.statusText)
      }
    }
  }

  let url = engine + '/ping?sitemap=' + sitemap_url
  xhr.open('GET', url, false)
  xhr.send()
  xhr.abort()
})
